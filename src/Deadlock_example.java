import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.Thread.sleep;


public class Deadlock_example {

    private int x,y,z = 0;
    private Lock lock1 = new ReentrantLock();
    private Lock lock2 = new ReentrantLock();

    public void foo(){

        System.out.println("============");
        System.out.println("Foo OP1:\n");
        System.out.println("============");

        System.out.println("OP1:- executing z operation.");
        z = z + 2;

        lock1.lock();
        System.out.println("OP1:- lock1 acquired\n" );
        System.out.printf("OP1:- values after lock1: X= %d, Y= %d, Z= %d \n",x,y,z);
        System.out.println("OP1:- executing x operation.");
        x = x + 2;

        System.out.printf("OP1:- values before lock2: X= %d, Y= %d, Z= %d \n",x,y,z);
        System.out.println("OP1:- Lock 2 acquired");
        lock2.lock();
        System.out.printf("OP1:- values after lock2: X= %d, Y= %d, Z= %d \n",x,y,z);

        System.out.printf("OP1:- values before unlock1: X= %d, Y= %d, Z= %d \n",x,y,z);
        lock1.unlock();
        System.out.println("OP1:- lock1 is released.");
        System.out.printf("OP1:- values after unlock1: X= %d, Y= %d, Z= %d \n",x,y,z);

        System.out.println("OP1:- executing y operation");
        y = y + 2;

        System.out.printf("OP1:- values before unlock2: X= %d, Y= %d, Z= %d \n",x,y,z);
        lock2.unlock();
        System.out.println("OP1:- lock2 is released.");
        System.out.printf("OP1:- values after unlock1: X= %d, Y= %d, Z= %d \n",x,y,z);
    }

    public void bar(){

        System.out.println("============");
        System.out.println("Bar OP2:\n");
        System.out.println("============");

        System.out.printf("OP2:- values before lock2: X= %d, Y= %d, Z= %d \n",x,y,z);
        lock2.lock();
        System.out.println("OP2:- lock2 acquired.");

        System.out.printf("OP2:- values after lock2: X= %d, Y= %d, Z= %d \n",x,y,z);

        y = y + 1;
        System.out.println("OP2:- executing y operation");

        System.out.printf("OP2:- values before lock1: X= %d, Y= %d, Z= %d \n",x,y,z);
        lock1.lock();
        System.out.println("OP2:- lock1 acquired");

        System.out.printf("OP2:- values after lock1: X= %d, Y= %d, Z= %d \n",x,y,z);

        x = x + 1;
        System.out.println("OP2:- executing x operation.");

        System.out.printf("OP2:- values before unlock1: X= %d, Y= %d, Z= %d \n",x,y,z);
        lock1.unlock();
        System.out.println("OP2:- lock1 realsed");
        System.out.printf("OP2:- values after unlock1: X= %d, Y= %d, Z= %d \n",x,y,z);

        System.out.printf("OP2:- values before unlock2: X= %d, Y= %d, Z= %d \n",x,y,z);
        lock2.unlock();
        System.out.println("OP2:- lock2 released");
        System.out.printf("OP2:- values after unlock2: X= %d, Y= %d, Z= %d \n",x,y,z);
    }

    public static void main(String[] args) {
        Deadlock_example deadlock = new Deadlock_example();
        new Thread(deadlock::foo, "OP1").start();
        new Thread(deadlock::bar, "OP2").start();
    }

}

